﻿using System;
using Wurk.Thrall;

namespace Wurk.Thane {
    public class Thane<TService> : Thrall<Thane<TService>>, IDisposable where TService : class, new() {

        public Thane() {
            // 
        }

        public void Dispose() {
            
        }
    }
}